package io.telenor.bustripper;

import org.glassfish.jersey.client.ClientConfig;

import java.net.URLEncoder;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;

/**
 * Searches for bus stops in area provided.
 */
public class FindBusStop implements Runnable {

    private static final String SEARCH_URL = "http://reisapi.ruter.no/Place/GetPlaces/";

    private String searchTerm;

    private Client client;

    private TripsCallback listener;

    public FindBusStop(TripsCallback callback, String searchTerm) {
        this.listener = callback;
        this.searchTerm = searchTerm;
    }

    public void run() {
        ClientConfig configuration = new ClientConfig();

        client = ClientBuilder.newClient(configuration);

        String urlEncodedSearchTerm;

        // sanitize user input (urlencode)
        try {
            urlEncodedSearchTerm = URLEncoder.encode(searchTerm, java.nio.charset.StandardCharsets.UTF_8.toString());
            // the ruter API does not support "+" it its query string
            // here I manually replace all spaces that have become "+" into "%20"
            urlEncodedSearchTerm = urlEncodedSearchTerm.replaceAll("\\+", "%20");
        } catch (Exception e) {
            System.out.print("couldn't urlencode searchTerm: " + e.getMessage());
            return;
        }
        System.out.println("DEBUG: " + urlEncodedSearchTerm);

        // prepending "/" seems to fix some problems (urls with periods)
        Invocation.Builder invocationBuilder = client.target(SEARCH_URL + urlEncodedSearchTerm + "/")
                .request(MediaType.APPLICATION_JSON);

        BusStopsCallBack callback = new BusStopsCallBack(listener);

        final AsyncInvoker asyncInvoker = invocationBuilder.async();
        asyncInvoker.get(callback);
    }
}
